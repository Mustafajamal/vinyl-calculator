<?php
/*
Plugin Name: vinyl calculator
Description: vinyl calculator buy online vinyl banners
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

			add_action( 'init', 'vinyl_calculator' );

			function vinyl_calculator() {

					add_shortcode( 'show_vinyl_calculator', 'vinyl_calculator_form' );
					add_action( 'wp_enqueue_scripts', 'vinyl_calculator_script' );
					add_action( 'wp_ajax_nopriv_post_love_set_product_canvas', 'post_love_set_product_canvas' );
					add_action( 'wp_ajax_post_love_set_product_canvas', 'post_love_set_product_canvas' );
			 }


			function my_enqueue() {

			    wp_enqueue_script( 'ajax-script', plugins_url() . '/vinyl_calculator/js/my-ajax-script.js', array('jquery') );

			    wp_localize_script( 'ajax-script', 'my_ajax_object',array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
			}
			add_action( 'wp_enqueue_scripts', 'my_enqueue' );


			 function vinyl_calculator_script() {
					        
				wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/vinyl_calculator/js/custom_fancy1.js',array(),time());	
				wp_enqueue_script( 'rt_masking_script', plugins_url().'/vinyl_calculator/js/masking.js');	
				
				wp_enqueue_style( 'rt_masking_css0', plugins_url().'/vinyl_calculator/css/custom_fancy.css');	
				wp_enqueue_style( 'rt_masking_css1', plugins_url().'/vinyl_calculator/smartforms/css/smart-forms.css');	
				wp_enqueue_style( 'rt_masking_css2', plugins_url().'/vinyl_calculator/smartforms/css/smart-addons.css');	
				wp_enqueue_style( 'rt_masking_css3', plugins_url().'/vinyl_calculator/smartforms/css/smart-themes/black.css');	
				wp_enqueue_style( 'rt_masking_css4', plugins_url().'/vinyl_calculator/smartforms/css/font-awesome.min.css');	
				
				//wp_enqueue_script( 'rt_masking_script1', plugins_url().'/vinyl_calculator/smartforms/js/jquery-1.9.1.min.js');
				wp_enqueue_script( 'rt_masking_script2', plugins_url().'/vinyl_calculator/smartforms/js/jquery.steps.min.js');
			    //wp_enqueue_script( 'rt_masking_script3', plugins_url().'/vinyl_calculator/smartforms/js/jquery-ui-custom.min.js');
			    //wp_enqueue_script( 'rt_masking_script4', plugins_url().'/vinyl_calculator/smartforms/js/jquery.validate.min.js');
			    // wp_enqueue_script( 'rt_masking_script5', plugins_url().'/vinyl_calculator/smartforms/js/additional-methods.min.js');
			    //wp_enqueue_script( 'rt_masking_script6', plugins_url().'/vinyl_calculator/smartforms/js/jquery-ui-slider-pips.min.js');
			    //wp_enqueue_script( 'rt_masking_script7', plugins_url().'/vinyl_calculator/smartforms/js/jquery-ui-touch-punch.min.js');
			    //wp_enqueue_script( 'rt_masking_script8', plugins_url().'/vinyl_calculator/smartforms/js/jquery.formShowHide.min.js');

			} 

		
			function post_love_set_product_canvas(){				
				 			
				//set product values:
				$new_post_id = 1112;
				//$rt_price = 3100;
				update_post_meta( $new_post_id, '_stock_status', 'instock');
				update_post_meta( $new_post_id, '_visibility', 'visible' );
				update_post_meta( $new_post_id, '_price', $_REQUEST['price']);
				update_post_meta( $new_post_id,'_regular_price',$_REQUEST['price'] );				
				
				update_post_meta( $new_post_id,'no_sides',$_REQUEST['no_sides']);				
				update_post_meta( $new_post_id,'meterial',$_REQUEST['meterial']);				
				update_post_meta( $new_post_id,'pole_pockets',$_REQUEST['pole_pockets']);				
				update_post_meta( $new_post_id,'hemming',$_REQUEST['hemming']);				
				update_post_meta( $new_post_id,'turnaround',$_REQUEST['turnaround']);				
				update_post_meta( $new_post_id,'grommets',$_REQUEST['grommets']);				
				update_post_meta( $new_post_id,'design_proof',$_REQUEST['design_proof']);						
				update_post_meta( $new_post_id,'front_side_url',$_REQUEST['front_side_url']);						
				update_post_meta( $new_post_id,'back_side_url',$_REQUEST['back_side_url']);						
				
				global $woocommerce;
				$woocommerce->cart->add_to_cart($new_post_id);
				$checkout_url = site_url().'/checkout';				
				
			}


			function bbloomer_redirect_checkout_add_cart( $url ) {
			    //$url = get_permalink( get_option( 'woocommerce_checkout_page_id' ) ); 
			    $url = $checkout_url = site_url().'/checkout';	
			    return $url;
			}
			 
			add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );						
			
			//Get the Data
	

			function vinyl_calculator_form() {
ob_start();				
		?>
    <script type="text/javascript">
	
		jQuery(document).ready(function($){
		
			jQuery("#smart-form").steps({
				bodyTag: "fieldset",
				headerTag: "h2",
				bodyTag: "fieldset",
				transitionEffect: "slideLeft",
				titleTemplate: "<span class='number'>#index#</span> #title#",
				labels: {
					finish: "Submit Form",
					next: "Continue",
					previous: "Go Back",
					loading: "Loading..." 
				}
				// onStepChanging: function (event, currentIndex, newIndex){
				// 	if (currentIndex > newIndex){return true; }
				// 	var form = $(this);
				// 	if (currentIndex < newIndex){}
				// 	return form.valid();
				// },
				// onStepChanged: function (event, currentIndex, priorIndex){
				// },
				// onFinishing: function (event, currentIndex){
				// 	//var form = $(this);
				// 	//form.validate().settings.ignore = ":disabled";
				// 	//return form.valid();
				// },
				// onFinished: function (event, currentIndex){
				// 	var form = $(this);
				// }
			});
			var home_url = document.location.href;
			var home_url_checkout = home_url+'checkout';
			console.log(home_url_checkout);
			jQuery("#proceed_checkout").attr("href", home_url_checkout);


				
		});    
    </script>     
  


  	<div class="smart-wrap">
    	<div class="smart-forms smart-container wrap-0">
        
            	<div class="form-body theme-black smart-steps steps-progress steps-theme-black">
                 <form method="post" action="" id="smart-form">
                            <h2>Build Your Job</h2>
                            <fieldset>
                            
                                <div class="frm-row">                                  
                                    
                                    <div class="section colm colm12">                      
										<div class="notification alert-custom">
										    <p class="thickness">Thickness: 13 oz</p>
										</div>
									</div><!-- end section -->

                                </div><!-- end .frm-row section -->    
                                                                                                
                                <div class="frm-row">
                                     
                                     <div class="section colm colm6">
                                        <label for="rt_width" class="field-label">Width (inches) </label>
                                        <label class="field prepend-icon">
                                        	<input onKeyup="getproductnamevbpukk()" placeholder="0.00"  type="text" name="rt_width" class="gui-input" id="rt_width" value="2" >	
                                            <span class="field-icon"><i class="fa fa-arrows-h"></i></span>  
                                        </label>
                                    </div><!-- end section -->

                                    <div class="section colm colm6">
                                        <label for="rt_height" class="field-label">Height (inches) </label>
                                        <label class="field prepend-icon">
                                            <input value="2" onKeyup="getproductnamevbpukk()"  placeholder="0.00" type="text" name="rt_height" class="gui-input" id="rt_height" required>
                                            <span class="field-icon"><i class="fa fa-arrows-v"></i></span>  
                                        </label>
                                    </div><!-- end section -->
                                    
                                </div><!-- end .frm-row section -->                                                                            
                                
                                <div class="frm-row">
                                 
                                    <div class="section colm colm6">
                                        <label for="rt_qty" class="field-label">Quantity</label>
                                        <label class="field prepend-icon">
                                            <input value="2" onchange="getproductnamevbpukk()"  placeholder="0" type="number" name="rt_qty" class="gui-input" id="rt_qty" required>
                                            <span class="field-icon"><i class="arrow double"></i></span>  
                                        </label>
                                    </div><!-- end section -->

                                    <div class="section colm colm6">
                                        <label for="rt_no_sides" class="field-label"># of Sides</label>
                                        <label class="field select">
                                            <select id="rt_no_sides" onchange="getproductnamevbpukk()" name="rt_no_sides">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                <i class="arrow double"></i>  
                                        </label>
                                    </div><!-- end section -->
                                </div> 
                                <div class="frm-row">
   	
                                    <div class="section colm colm6">
                                        <label for="rt_meterial" class="field-label">Meterial</label>
                                        <label class="field select">
                                            <select id="rt_meterial"  onchange="getproductnamevbpukk()" name="rt_meterial">
                                                    <option value="13_oz_matte">13 oz Matte	Vinyl Banner</option>
                                                    <option value="14_oz_matte">14 oz Matte	Vinyl Banner</option>
                                                </select>
                                                <i class="arrow double"></i>  
                                        </label>
                                    </div><!-- end section -->

                                    <div class="section colm colm6">
                                       <label for="rt_pole_pocket" class="field-label">Pole Pocket</label>
                                       <label class="field select">
                                            <select id="rt_pole_pocket"  onchange="getproductnamevbpukk()" name="rt_pole_pocket">
                                                    <option value="no_pole_pockets">No Pole Pockets</option>
                                                    <option value="all_pole_pockets">All Sides</option>
                                                </select>
                                                <i class="arrow double"></i>  
                                        </label>
                                    </div><!-- end section -->
                                 </div><!-- end .frm-row section -->
                                   
                                
                                <div class="frm-row">                               	
		                                
                                    <div class="section colm colm6">
                                        <label for="rt_hem" class="field-label">Hemming</label>
                                        <label class="field select">
                                            <select id="rt_hem"  onchange="getproductnamevbpukk()" name="rt_hem">
                                                    <option value="all_sides">All Sides</option>
                                                    <option value="all_sides1">All Sides 1</option>
                                                </select>
                                                <i class="arrow double"></i>  
                                        </label>
                                    </div><!-- end section -->    
                                    <div class="section colm colm6">
                                        <label for="rt_turnaround" class="field-label">Turnaround</label>
                                        <label class="field select">
                                            <select id="rt_turnaround"  onchange="getproductnamevbpukk()" name="rt_turnaround">
                                                    <option value="next_day">Next Day</option>
                                                    <option value="48_hours">48 Hours</option>
                                                </select>
                                                <i class="arrow double"></i>  
                                        </label>
                                    </div><!-- end section -->
								</div> 

								<div class="frm-row">                               	
		                              
                                    <div class="section colm colm6">
                                        <label for="rt_design_proof" class="field-label">Design Proof</label>
                                        <label class="field select">
                                            <select id="rt_design_proof" onchange="getproductnamevbpukk()" name="rt_design_proof">
                                                    <option value="no_proof">No Proof Run as is</option>
                                                    <option value="has_proof">Has Proof</option>
                                                </select>
                                                <i class="arrow double"></i>  
                                        </label>
                                    </div><!-- end section -->
									  <div class="section colm colm6">
                                        <label for="rt_design_proof" class="field-label">Grommets</label>
                                        <label class="field select">
											<select name="rt_grommets" id="rt_grommets" onchange="getproductnamevbpukk()">
												<option value="Corners Only">Grommets in Corners Only</option>
												<option value="No Grommets">No Grommets</option>
												<option value="Every 2 Ft">Grommets Every 2 Ft.</option>
											</select>
                                                <i class="arrow double"></i>  
                                        </label>
                                    </div><!-- end section -->
								</div>
								<div class="frm-row">                               	
		                              
								<div class="section colm colm6">
								<label for="rt_design_proof" class="field-label">Front page</label>					
									<?php echo do_shortcode('[ajax-file-upload on_success_set_input_value="#front_side_url"]'); ?>
									<span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span>
									<input type="text" id="front_side_url" value="" class="gui-input">
									</div>
									<div class="section colm colm6">
									<label for="rt_design_proof" class="field-label">Back page</label>	
									<?php echo do_shortcode('[ajax-file-upload on_success_set_input_value="#back_side_url"]'); ?>

									<span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span>
									<input type="text" id="back_side_url" value="" class="gui-input">
									 </div><!-- end section -->
								</div>
	                                
	                                <div class="section colm colm12">
		                                    <label for="pricetotal_below" class="field-label">Price Per Piece</label>
		                                    <input class="per_piece_price" id="pricetotal_below" name="pricetotal_below" placeholder="$0.00" disabled>
		                                    <div id="sqft_rate"><span id="total_sqfeet"></span> SQFT @ <span id="per_sqfeetrate">$2.59</span></div>
		                            </div><!-- end section -->                                                                 
                                

                            </fieldset>
            
                            <h2>Add Job to Cart</h2>
                            <fieldset>
                            
                                <?php //echo do_shortcode('[woocommerce_cart]'); ?>	
                                <div class="frm-row">
                                    <div class="section colm colm12">
                                        <h3 id="step_2price_total"></h3>
                                        <a id="add_to_cart11" onClick="add_to_cart()" class="button btn-black"> Add Job to Cart </a>
                                    </div><!-- end section -->
                                	             
                                </div><!-- end frm-row section -->
                                
                            </fieldset>
                            
                   
                           <h2>Shipping and Billing</h2>
                            <fieldset>
                                
                                
                                <h2>Proceed to Checkout</h2>	

								<a id="proceed_checkout" href="#" class="button btn-black"> Proceed to Checkout </a>
                            </fieldset>   


                    </form>                                                                                   
                </div><!-- end .form-body section -->
            
        </div><!-- end .smart-forms section -->
    </div><!-- end .smart-wrap section -->

<?php 
$result = ob_get_clean();
return $result;
//Add meta to cart

//final bracket
}
add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);
add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);
function order_meta_handler($item_id, $values, $cart_item_key) {
			$cart_session = WC()->session->get('cart');
			//wc_add_order_item_meta($item_id, "ghjjhgjh Sides", 'gjghjgjgjhg');

			if($cart_session[$cart_item_key]['no_sides'][0]){
			wc_add_order_item_meta($item_id, "No Sides", $cart_session[$cart_item_key]['no_sides']);
			}
			if($cart_session[$cart_item_key]['meterial'][0]){
			wc_add_order_item_meta($item_id, "Meterial", $cart_session[$cart_item_key]['meterial']);
			}
			if($cart_session[$cart_item_key]['pole_pockets'][0]){
			wc_add_order_item_meta($item_id, "Pole Pockets", $cart_session[$cart_item_key]['pole_pockets']);
			}
			if($cart_session[$cart_item_key]['hemming'][0]){
			wc_add_order_item_meta($item_id, "Hemming", $cart_session[$cart_item_key]['hemming']);
			}
			if($cart_session[$cart_item_key]['turnaround'][0]){
			wc_add_order_item_meta($item_id, "Turnaround", $cart_session[$cart_item_key]['turnaround']);
			}
			if($cart_session[$cart_item_key]['grommets'][0]){
			wc_add_order_item_meta($item_id, "Grommets", $cart_session[$cart_item_key]['grommets']);
			}
			if($cart_session[$cart_item_key]['design_proof'][0]){
			wc_add_order_item_meta($item_id, "Design Proof", $cart_session[$cart_item_key]['design_proof']);
			}
			if($cart_session[$cart_item_key]['front_design11'][0]){
			wc_add_order_item_meta($item_id, "Front design", $cart_session[$cart_item_key]['front_design11']);
			}
			if($cart_session[$cart_item_key]['back_design11'][0]){
			wc_add_order_item_meta($item_id, "Back design", $cart_session[$cart_item_key]['back_design11']);			
			}
			
			//wc_add_order_item_meta($item_id, "Front design", 'sfdfsfsff');
			
			
		}
function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
	global $woocommerce;
	//Get the Data
	$no_sides = get_post_meta( $new_post_id, 'no_sides');
	$meterial = get_post_meta( $new_post_id,'meterial',$_REQUEST['meterial']);				
	$pole_pockets =	get_post_meta( $new_post_id,'pole_pockets',$_REQUEST['pole_pockets']);				
	$hemming =	get_post_meta( $new_post_id,'hemming',$_REQUEST['hemming']);				
	$turnaround =	get_post_meta( $new_post_id,'turnaround',$_REQUEST['turnaround']);				
	$grommets =	get_post_meta( $new_post_id,'grommets',$_REQUEST['grommets']);				
	$design_proof =	get_post_meta( $new_post_id,'design_proof',$_REQUEST['design_proof']);		
	$front_design =	get_post_meta( $new_post_id,'front_side_url',$_REQUEST['design_proof']);		
	$back_design =	get_post_meta( $new_post_id,'back_side_url',$_REQUEST['design_proof']);		
	//$front_design =	get_post_meta( $new_post_id,'front_side_url');	
	//$front_design ='XZXZXXzX';	
	$cart_item_meta['no_sides'] = $no_sides;
	$cart_item_meta['meterial'] = $meterial;
	$cart_item_meta['pole_pockets'] = $pole_pockets;
	$cart_item_meta['hemming'] = $hemming;
	$cart_item_meta['grommets'] = $grommets;
	$cart_item_meta['turnaround'] = $turnaround;
	$cart_item_meta['design_proof'] = $design_proof;
	$cart_item_meta['front_design11'] = $front_design;
	$cart_item_meta['back_design11'] = $back_design;
	return $cart_item_meta;
}